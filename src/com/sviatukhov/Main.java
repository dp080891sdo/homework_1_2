package com.sviatukhov;

import java.util.Scanner;

public class Main {
    /*
    Задача 2
    Пользователь вводит координаты точек
    | введите координаты точки
    | x: 10
    | y: 20
    | Желаете добавить еще (1-да 2- нет)
    | ваш выбор: 1
    | введите координаты точки
    | x: 11
    | y: 4
    | Желаете добавить еще (1-да 2- нет)
    | ваш выбор: 2
    Затем программа спрашивает координаты центра и радиус окружности
    Вывести только те точки, которые лежат в окружности
    Примечание. Для получения оценки 95 и выше должны быть созданы минимум 3 класса Point , Circle и PointList (список точек)
    Важные подсказки:
    центр окружности - это точка, не забывайте об этом
    точка лежит в окружности, если расстояние от центра окружности до точки меньше радиуса окружности
    расстояние между точками насчитывается по формуле d = корень((x1-x2)^2 + (y1-y2)^2),
     где x1,y1- координаты первой точки, x2,y2- координаты второй
     */

    public static final int MAX_COUNT = 999;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int answer = 1;
        for (int i = 0; i <= MAX_COUNT; i++) {
            if (answer == 1) {
                System.out.println("введите координаты Х точки");
                int x = scanner.nextInt();
                System.out.println("введите координаты Y точки");
                int y = scanner.nextInt();
                Point point = new Point(x, y);
                PointList.addPoint(point);
                System.out.println("Желаете добавить еще (1-да 2- нет)");
                answer = scanner.nextInt();
            }
            if (answer > 2 || answer < 1) {
                System.out.println("Ответ должен быть 1 или 2. Введите корректный ответ. Желаете добавить еще (1-да 2- нет)");
                answer = scanner.nextInt();
            } else if (answer == 2) {
                break;
            }
        }
        System.out.println("Введите координаты X центра окружности");
        int x = scanner.nextInt();
        System.out.println("Введите координаты Y центра окружности");
        int y = scanner.nextInt();
        System.out.println("Введите радиус окружности");
        int radius = scanner.nextInt();
        Circle circle = new Circle(x, y, radius);
        System.out.println("Список точек в окружности");
        PointList.getPointList(circle);
    }
}
