package com.sviatukhov;

import java.util.ArrayList;
import java.util.List;

public class PointList {
    static List<Point> pointList = new ArrayList<>();
    static List<Point> finalPointList = new ArrayList<>();

    public static void addPoint(Point point) {
        pointList.add(point);
    }

    public static void getPointList(Circle circle) {
        for (Point point : pointList) {
            double d = Math.sqrt(Math.pow((point.x - circle.x), 2) +
                    Math.pow((point.y - circle.y), 2));
            if (d < circle.radius) {
                createFinalList(point);
            }
        }
        System.out.println(finalPointList);
    }

    private static void createFinalList(Point point) {
        finalPointList.add(point);
    }
}